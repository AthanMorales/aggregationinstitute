/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package institute;

import java.util.List;

/**
 *
 * @author Mauricio
 */
public class Department {
    private String departmentName;
    private List<Student> students;

    public Department(String departmentName, List<Student> students) {
        this.departmentName = departmentName;
        this.students = students;
    }

    public String getDeparmentName() {
        return departmentName;
    }

    public void setDeparmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
